`Back to Numerics Course Home Page <http://mpecdt.bitbucket.org/>`_

Copy of the Announcements sent by email
=============================================================

* Friday 30 Sept 2016

Looking forward to teaching you all numerics this term and looking forward to seeing you on Wednesday at 3:30pm (Claire <c.e.l.morris@reading.ac.uk> will let us know the location of this class soon). I teach the first half of this course and Colin Cotter teaches the second half. I will be using a "flipped classroom" which means that you go through the lecture material before class without me and during the classes we work on exercises from the lecture notes and on the practicals. Therefore before each of the classes there will be reading to be done and (optionally) teaching videos to watch. Details of what you should read before Wednesday 5 October is at:

http://mpecdt.bitbucket.org/

Then during the classes on Wednesday 5 and Thursday 6 October you can work on exercises in the lecture notes and on assignment 1. Note that the deadline for assignment 1 is Monday 10 October and this is worth 10% of the module total.

Some of the classes will be on Wednesdays when we will all be together. During these classes I will give priority to the Imperial students. Some of the classes will be on Thursday afternoon over video link. I will attempt to interact with everyone on Thursday afternoons but communication with Reading students will be easier. 

If you have questions about the module, including the assignments, post them on the discussion cite:

https://share.imperial.ac.uk/fons/mathematics/mpecdt/
students/Lists/TeamDiscussion/

I will answer questions promptly. You are also encouraged to try to answer questions that other people ask and check to see if anyone has asked the same question.

Remember to bring your laptops to all classes.

Finally, please reply to confirm that you have received this email.

Many thanks and looking forward to seeing you

Hilary
